function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('.sub-nav-top').offset().top;
    if (window_top > div_top) {
        $('.sub-nav-cat').addClass('stick');
        $('.sub-nav-top').height($('.sub-nav-cat').outerHeight());
    } else {
        $('.sub-nav-cat').removeClass('stick');
        $('.sub-nav-top').height(0);
    }
}



$(document).ready(function () {
    $(window).scroll(sticky_relocate);

    sticky_relocate();
    $("a.page-scroll").click(function (e) {
        var $this = $(this);
        var id = $this.attr('href');
        $('.sub-nav-cat').addClass('fixed');
        $('html,body').animate({
            scrollTop: $(id).offset().top
        }, 800);
    });


    $(".multiple").slick({
        autoplaySpeed: 2500,
        infinite: true,
        slidesToShow: 2,
        autoplay: true,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 430,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
});